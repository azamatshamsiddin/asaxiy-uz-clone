import React from "react";
import { Link, Outlet } from "react-router-dom";
import "./header.scss";
import { Logo, Search } from "../../assets";
import {
  BiUser,
  MdOutlinePayment,
  MdOutlineShoppingCart,
  TbTruckDelivery,
  TiHeartOutline,
  VscGlobe
} from "react-icons/all";

const Header = () => {
  return (
    
    <>
      <header className="header">
        <div className="header__top border-bottom">
          <div className="container d-flex align-items-center py-3">
            <Link to='/home' className="header__logo">
              <Logo/>
            </Link>
            <div className="header__search d-flex flex-fill">
              <form className="search__form">
                <input className="search__input text-primary border-primary " type="text" placeholder="Qidirish..."/>
                <button className="search__btn bg-primary text-white border-0 d-flex align-items-center shadow-none">
                  <span className="position-relative"><Search/></span>Qidirish
                </button>
              </form>
            </div>
            <ul className="ms-auto my-auto gap-4 d-flex align-items-center justify-content-between ">
              <li>
                <Link to="/" className="d-flex flex-column align-items-center">
                  <MdOutlinePayment size={30}/>
                  <span>To'lov</span>
                </Link>
              </li>
              <li>
                <Link to="/" className="d-flex flex-column align-items-center">
                  <TbTruckDelivery size={30}/>
                  <span>Trek</span>
                </Link>
              </li>
              <li>
                <Link to="/" className="d-flex flex-column align-items-center ">
                  <VscGlobe size={30}/>
                  <span>O'zbekcha</span>
                </Link>
              </li>
              <li>
                <Link to="/" className="d-flex flex-column align-items-center">
                  <MdOutlineShoppingCart size={30}/>
                  <span>Savatcha</span>
                </Link>
              </li>
              <li>
                <Link to="/" className="d-flex flex-column align-items-center">
                  <TiHeartOutline size={30}/>
                  <span>Sevimlilar</span>
                </Link>
              </li>
              <li>
                <Link to="/" className="d-flex flex-column align-items-center">
                  <BiUser size={30}/>
                  <span>Kabinet</span>
                </Link>
              </li>
            
            </ul>
          </div>
        </div>
        <div className="header__bottom container">
          <nav className="">
          <ul className="d-flex align-items-center justify-content-between">
            <li>
              <Link to="/">Barcha bo'limlar</Link>
            </li>
            <li>
              <Link to="/">Yangiliklar</Link>
            </li>
            <li>
              <Link to="/">Yangi kelganlar</Link>
            </li>
            <li>
              <Link to="/">Chegirmalar</Link>
            </li>
            <li>
              <Link to="/">Bo'lib to'lash</Link>
            </li>
            <li>
              <Link to="/">Kitoblar</Link>
            </li>
            <li>
              <Link to="/">Mebel</Link>
            </li>
            <li>
              <Link to="/">Telefonlar va gadjetlar</Link>
            </li>
            <li>
              <Link to="/">Konditsionerlar</Link>
            </li>
          </ul>
          </nav>
        </div>
       
      </header>
      
      <Outlet/>
    </>
  );
};

export default Header;
