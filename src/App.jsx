import React from "react";
import Header from "./components/header/Header";
import { Route, Routes } from "react-router-dom";
import Main from "./components/Main/Main.jsx";

function App() {
  return (
    <>
      <Routes>
        <Route path={"/"} element={<Header/>}>
          <Route path={"home"} element={<Main/>}/>
        </Route>
      </Routes>
    </>
  
  );
}

export default App;
